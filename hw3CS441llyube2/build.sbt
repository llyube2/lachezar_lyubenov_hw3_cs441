name := "hw3CS441llyube2"

version := "0.1"

scalaVersion := "2.11.8"

val sparkVersion = "2.0.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "com.github.piotr-kalanski" % "csv2class_2.11" % "0.3.3",
  "com.typesafe" % "config" % "1.2.1",
  "org.scalaj" % "scalaj-http_2.11" % "2.3.0",
  "org.apache.spark" %% "spark-hivecontext-compatibility" % "2.0.0-preview",
  "com.github.nscala-time" %% "nscala-time" % "1.8.0",
  "com.chuusai" %% "shapeless" % "2.3.3",
  compilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
  "com.holdenkarau" %% "spark-testing-base" % "2.4.0_0.11.0" % Test,
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"

)

resolvers += "jitpack" at "https://jitpack.io"


assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}