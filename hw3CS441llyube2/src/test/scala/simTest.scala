import junit.framework.TestCase
import monteCarloSim.prepareData
import org.apache.spark.sql
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{lag, when}
import org.apache.spark.sql.types._

/*
 * HW3 - CS441 - Lachezar Lyubenov - llyube2
 * Cloud Computing
 * Testing suite for my hw3 assignment
 */

class simTest extends TestCase{

  def initSpark(spark: sql.SparkSession): sql.DataFrame = {

    /*
     * Defining custom schema for DataFrame
     */
    val customSchema = StructType(Array(
      StructField("Date", DateType, true),
      StructField("Symbol", StringType, true),
      StructField("High", DoubleType, true),
      StructField("Low", DoubleType, true),
      StructField("Open", DoubleType, true),
      StructField("Close", DoubleType, true)))

    /*
     * Init for DataFrame
     * Reading .csv file with companies
     */
    val df = spark.read
      .format("csv")
      .option("header", "true") //reading the headers
      .schema(customSchema)
      .option("mode", "DROPMALFORMED")
      .csv("src/main/resources/financialHistoryData.csv")

    /*
     * For local uncomment the line below and remove .csv with s3a path.
     *  .csv("src/main/resources/financialHistoryData.csv")
     *  .csv("s3a://financial-history-data/financialHistoryData.csv")
     */

    /*
     * Adding Price Change column to the DataFrame
     */
    import spark.implicits._
    val windowSpec = Window.partitionBy("Symbol").orderBy("Date")
    val dfWithChange: sql.DataFrame = df.withColumn("Change", $"Close" - when((lag("Close", 1).over(windowSpec)).isNull, 0).otherwise(lag("Close", 1).over(windowSpec)))
    dfWithChange
  }

  /*
   * Testing initSpark function to make sure that result is not NULL
   */
  def testInit: Unit ={
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;
    val dfWithChange: sql.DataFrame = initSpark(spark)

    assert(dfWithChange != null, "init function should not be null")

  }

  /*
   * Testing if the predefined schema is correct with correct Column names
   */
  def testSchema: Unit = {
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;
    val df: sql.DataFrame = initSpark(spark)

    assert(df.col("Date").toString() == "Date", "Correct column name should be /Date/")
    assert(df.col("Symbol").toString() == "Symbol", "Correct column name should be /Symbol/")
    assert(df.col("High").toString() == "High", "Correct column name should be /High/")
    assert(df.col("Low").toString() == "Low", "Correct column name should be /Low/")
    assert(df.col("Open").toString() == "Open", "Correct column name should be /Open/")
    assert(df.col("Close").toString() == "Close", "Correct column name should be /Close/")

  }

  /*
   * Testing single simulation run with pre-calculated result
   */
  def testSimulation: Unit ={
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;
    initSpark(spark).show
    val testArray = Array(1.0, 2.0 ,3.0, 4.0, 5.0)
    val computedTotal = monteCarloSim.maxProfitGivenK(5, 5, testArray)
    assert(computedTotal.toString == "16000.0", "Pre-calculated amount from maxProfitGivenK should be /16000.00/")
  }

  /*
   * Testing if adding columns to DataFrame is doing it correctly
   */
  def testAddingtoDF: Unit = {
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;

    val dfWithChange: sql.DataFrame = initSpark(spark)
    prepareData(spark, dfWithChange, 5)
    assert(dfWithChange.col("Change").toString() == "Change", "Additional column with name /Change/ expected")

  }

  /*
   * Testing of prepareData function with pre-calculated data
   */
  def testPrepareData: Unit ={
    val spark = org.apache.spark.sql.SparkSession.builder
      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;

    val dfWithChange: sql.DataFrame = initSpark(spark)
    val computedAmount = prepareData(spark, dfWithChange, 5)

    assert(computedAmount.round == 23353, "Pre-calculated amount as double expected to be 23353.492")
  }

}