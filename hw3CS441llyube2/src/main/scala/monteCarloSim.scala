import org.apache.spark.sql
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.joda.time.{DateTime, Days}
import scala.collection.mutable.ArrayBuffer

/*
 * HW3 - CS441 - Lachezar Lyubenov - llyube2
 * Cloud Computing
 *  Assignment: Financial Simulation based on Monte Carlo
 *       Using: Spark, Scala
 *    Deployed: AWS EMR
 * Exploring the world of Scala, Spark and AWS EMR to build max profit algorithm
 * based on historical data from Yahoo financial.
 */


/*
 * Object that has the configuration
 *  for the simulation
 */
object CONFIGURATION {

  val startDate: String = "2018-10-10"
  val endDate: String = "2018-10-31"
  val availableMoney: Double = 20000.00
  val numSimulations: Int = 25

}


object monteCarloSim {


  /*
   * Function that parses the date
   *  from the configuration and
   *  calculates the DateRange for which
   *  the simulation would run for.
   */
  def dateConversion(): IndexedSeq[DateTime] = {

    val start = DateTime.parse(CONFIGURATION.startDate)
    val end = DateTime.parse(CONFIGURATION.endDate)
    val numberOfDays = Days.daysBetween(start, end).getDays()
    val dates: IndexedSeq[DateTime] = for (f <- 0 to numberOfDays) yield start.plusDays(f)

    dates
  }


  /*
   * Function that creates a table to:
   *  Calculate possible profits given
   *  random amount of trades available
   *  returns the profit for given stock
   */
  def maxProfitGivenK(trades: Int, companies: Int, prices: Array[Double]): Double = {


    if (prices.size < 2 || trades == 0) {
      return 0.0
    }
    else {
      val maxTransaction = Math.min(trades, prices.size / 2)
      val oddTerm = ArrayBuffer.fill(maxTransaction)(Double.MinValue)
      val evenTerm = ArrayBuffer.fill(maxTransaction)(0.0)
      val availableMoney = CONFIGURATION.availableMoney.toDouble / companies
      val amountInvested = availableMoney.toDouble / prices(0)
      for (i <- prices.indices) {
        val price = prices(i)
        for (j <- 0 until maxTransaction) {
          if (j == 0) {
            oddTerm(j) = Math.max(oddTerm(j), -price)
          } else {
            oddTerm(j) = Math.max(oddTerm(j), evenTerm(j - 1) - price)
          }
          evenTerm(j) = Math.max(evenTerm(j), oddTerm(j) + price)
        }
      }
      evenTerm(maxTransaction - 1).toDouble * amountInvested
    }
  }

  def prepareData(spark: sql.SparkSession, df: sql.DataFrame, trades: Int): Double = {

    /*
     * Creating columns within DataFrame for:
     *  DailyChange of Stock Price
     *  CurrentInvestmentTotal
     */
    val newDF = df.withColumn("InvestmentChangeDaily", (col("Change") * 100.00))
    newDF.withColumn("CurrentInvestmentTotal", col("InvestmentChangeDaily").plus(100.00))

    /*
     * Creating list of Dates based on
     *  the correct dates specified by user.
     *  Making sure that its Seq[String]
     */
    val listOfDates: IndexedSeq[DateTime] = dateConversion()
    val correctListOfDates = listOfDates.map(_.toString().substring(0, 10))


    /*
     * Mapping from the DataFrame so:
     *  The array contains [(Symbol, (Date, Close))]
     *  Then grouping and mapping by Symbol so I have Collections x n of stocks specified by user
     */
    val symbolAsKeyTuple = newDF.select("Date", "Symbol", "Close").rdd.map(x => (x.get(1), new Tuple2(x.get(0), x.get(2)))).collect()
    val groupBySymbol = symbolAsKeyTuple.groupBy(_._1).map { case (k, v) => k -> v.map(_._2).toList }
    val mapBySymbol = groupBySymbol.map { outer => (outer._1, outer._2.filter(inner => correctListOfDates.contains(inner._1.toString))) }
    val numCompanies = mapBySymbol.keys.size
    /*
     * Sending grouped data "mapBySymbol" to:
     *  maxProfitGivenK function that takes:
     *  Closing Cost
     *  N of trades allowed
     */

    val maxProfitWithK = mapBySymbol.map(x => maxProfitGivenK(trades, numCompanies, x._2.map(m => m._2.asInstanceOf[Double]).toArray))

    /*
     * Summing the output from algorithm
     *  (maxProfitWithK.sum) + User specified money
     */
    val maxSum = maxProfitWithK.sum + CONFIGURATION.availableMoney
    maxSum
  }

  /*
   * Function that init's Spark:
   *  Defines custom schema
   *  Reads from .csv file (Historical Data)
   *  Adds additional column (Price Change) to DataFrame
   */
  def initSpark(spark: sql.SparkSession): sql.DataFrame = {

    /*
     * Defining custom schema for DataFrame
     */
    val customSchema = StructType(Array(
      StructField("Date", DateType, true),
      StructField("Symbol", StringType, true),
      StructField("High", DoubleType, true),
      StructField("Low", DoubleType, true),
      StructField("Open", DoubleType, true),
      StructField("Close", DoubleType, true)))

    /*
     * Init for DataFrame
     * Reading .csv file with companies
     */
    val df = spark.read
      .format("csv")
      .option("header", "true") //reading the headers
      .schema(customSchema)
      .option("mode", "DROPMALFORMED")
      .csv("s3a://financial-history-data/financialHistoryData.csv")

    /*
     * For local uncomment the line below and remove .csv with s3a path.
     *  .csv("src/main/resources/financialHistoryData.csv")
     *  .csv("s3a://financial-history-data/financialHistoryData.csv")
     */

    /*
     * Adding Price Change column to the DataFrame
     */
    import spark.implicits._
    val windowSpec = Window.partitionBy("Symbol").orderBy("Date")
    val dfWithChange: sql.DataFrame = df.withColumn("Change", $"Close" - when((lag("Close", 1).over(windowSpec)).isNull, 0).otherwise(lag("Close", 1).over(windowSpec)))
    dfWithChange
  }

  def main(args: Array[String]) {

    /*
     * Initiating SparkSession
     *
     */
    val spark = org.apache.spark.sql.SparkSession.builder
//      .master("local[*]")
      .appName("Spark CSV Reader")
      .getOrCreate;
    val dfWithChange: sql.DataFrame = initSpark(spark)

    /*
     * Variables needed for simulation Loop:
     */
    var simulation = 1
    var numTrades = 1
    var listOfProfits: List[(Int, Double)] = List()

    /*
     * While loop to run the desired number of simulations
     */
    while (simulation <= CONFIGURATION.numSimulations) {

      listOfProfits = listOfProfits :+ (simulation, prepareData(spark, dfWithChange, numTrades))
      simulation = simulation + 1
      numTrades = numTrades + 1
    }

    /*
     * Using the list of profits generated by running all simulations,
     * a new DataFrame is build on which mapping is done to
     * establish mean, max and min for all possible profits
     */
    import spark.implicits._
    val dfWithColNames: DataFrame = listOfProfits.toDF("Simulations", "Total")
    val Statistics = dfWithColNames.dropDuplicates("Total").describe("Total", "Simulations").select("summary", "Total", "Simulations").rdd.map(x => (x.get(0), x.get(2), x.get(1))).collect()

    /*
     * Result printing
     * for profits:
     * min, max and mean
     */
    println("------------------------------------------------------------------------------")
    println("Statistics: ")
    println("------------------------------------------------------------------------------")
    println("Maximum Return: " + listOfProfits.maxBy(_._2)._2 + " with " + listOfProfits.maxBy(_._2)._1 + " trades.")
    println("Minimum Return: " + listOfProfits.minBy(_._2)._2 + " with " + listOfProfits.minBy(_._2)._1 + " trades.")
    print("Average trades: ")
    Statistics.foreach(x => {
      if (x._1 == "mean")
        println(x._2)
    })
    print("Mean: ")
    Statistics.foreach(x => {
      if (x._1 == "mean")
        println(x._3)
    })
    println("------------------------------------------------------------------------------")
    println("------------------------------------------------------------------------------")

  }
}
