import com.typesafe.config.Config

class Settings(config: Config) extends Serializable {
  val startDate: String = config.getString("startDate")
  val endDate: String = config.getString("endDate")

  val availableMoney: Int = config.getInt("availableMoney")
  val numTrades: Int = config.getInt("numTrades")
  val numSimulations: Int = config.getInt("numSimulations")

}