import pandas_datareader.data as web
import datetime


# Small python script that uses panda's datareader to pull data from
# Yahoo based on text file that specifies company tickers.


#read ticker symbols from a file to python symbol list
symbol = []
with open('companies.txt') as f:
    for line in f:
        symbol.append(line.strip())
f.close


#datetime is a Python module for Date manipulalations

end = datetime.datetime.today()


### Change the number "20" in the line below to change how many years
### stock data to write to the .csv file. 20 = 20 years back
start = datetime.date(end.year-20,1,1)

# main loop to get data from yahoo
i=0
while i<len(symbol):
    try:
        df = web.DataReader(symbol[i], 'yahoo', start, end)
        df.insert(0,'Symbol',symbol[i])
        df = df.drop(['Adj Close'], axis=1)
        df = df.drop(['Volume'], axis=1)
        if i == 0:
            df.to_csv('financialHistoryData.csv')
            print (i, symbol[i],'has data stored to csv file')
        else:
            df.to_csv('financialHistoryData.csv',mode = 'a',header=False)
            print (i, symbol[i],'has data stored to csv file')
    except:
        print("No information for ticker # and symbol:")
        print (i,symbol[i])
        continue
    i=i+1