# Homework 3
### Description: you will gain experience with the Spark computational model in AWS cloud datacenter.
#### Name: Lachezar Lyubenov – NetID: Llyube2

## How to run

Getting the financial data is simple (video shows this):

1.    Navigate to the src/main/resources folder.
2.    Open folder using VS Code or any other coding text editors .
3.    In the folder there are “Companies.txt” and “getFinancialData.py”.
4.    Edit Companies.txt with the desired Stock Market Tickers.
5.    Edit the desired timeframe by changing the value of the datetime.date function where the “??” are.
       start = datetime.date(end.year-??,1,1)
6.    Using terminal or windows shell run python file by invoking the following command:
python getFinancialData.py
7.    This will generate .csv file with the desired historical data.

Running my program on AWS EMR (Video shows this):

1.    Once I have the .csv file, open my project in IntelliJ
2.    Using the sbt command shell clean the project by running the following command: clean
3.    Also there run: compile
4.    Also there run: package
5.    This will create a .jar file of the project in the (“targer/scala-2.11”) folder
6.    Login to AWS and locate the S3 page
7.    Within S3, upload the .csv file with created using the python script.
8.    Also there, upload the .jar file with packages from the opened project
9.    Create an EMR cluster
10.    SSH into it using the command generated on the cluster’s page



11.    Copy the .jar file to EMR using the following command:
aws s3 cp s3://financial-history-data/hw3cs441llyube2_2.11-0.1.jar .

NOTE: “financial-history-data” is the folder on s3 where you uploaded the .jar and .csv files.

12.    Run the following command to execute the .jar file on the EMR:
spark-submit –master yarn ./hw3cs441llyube2_2.11-0.1.jar

Output is generated displaying Minimum, maximum and Mean will be displayed on the screen:


##For in detail explanation of how to run the program, please see my video.

#Creating .csv, compiling, packaging, uploading and running on AWS EMR
Link: https://youtu.be/vlX8Vl6pr_4


#Configurating the Date range, available money and number of simulations explanation:
Link: https://youtu.be/ZydmaPec6wM

